# Android Mobile Challenge

This app was developed using Clean Architecture + MVVM.

It used Coroutines and LiveData for asynchronous tasks and Koin for dependency injection. 

To make the service layer, Retrofit and OkHttp was used.

A sealed class was created to handle the the service response. The GetHeroUseCase is used to retrived data and convert the response object to an object that the view could interprete.

Picasso is used to load images from url into the views

### Tests

I didnt implement tests for this project because I'm still studing the rigth way the make tests in this architecture, how to test suspend functions and how to handle the sealed class asserts