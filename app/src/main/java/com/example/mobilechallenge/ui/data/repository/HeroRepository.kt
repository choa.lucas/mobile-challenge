package com.example.mobilechallenge.ui.data.repository

import com.example.mobilechallenge.service.Outcome
import com.example.mobilechallenge.ui.data.model.HeroesResponse


interface HeroRepository {
    suspend fun getHeroByYear(year: Int): Outcome<HeroesResponse>
}