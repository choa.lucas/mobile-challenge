package com.example.mobilechallenge.ui.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.mobilechallenge.R
import com.example.mobilechallenge.ui.domain.model.Hero
import com.example.mobilechallenge.ui.presentation.adapter.HeroesAdapter
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class MainFragment : Fragment() {

    private val viewModel: MainViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        addObserverListeners()
        addViewListener()

        viewModel.getNextHero(viewModel.selectNextYear())
    }

    private fun addViewListener() {
        srChangeYear.setOnRefreshListener {
            viewModel.getNextHero(viewModel.selectNextYear())
        }
    }

    private fun addObserverListeners() {
        viewModel.hero.observe(this, Observer {
            srChangeYear.isRefreshing = false
            populateList(it)
        })

        viewModel.year.observe(this, Observer {
            tvYear.text = it
        })

        viewModel.errorCode.observe(this, Observer {
            showHttpStatusCodeError(it)
        })
    }

    private fun showHttpStatusCodeError(httpStatusCode: Int) {
        Toast.makeText(
            requireContext(),
            getString(R.string.service_error, httpStatusCode),
            Toast.LENGTH_LONG
        ).show()
    }

    private fun populateList(heroes: List<Hero>) {
        rvContent.removeAllViews()
        rvContent.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        rvContent.adapter = HeroesAdapter(heroes)
    }
}
