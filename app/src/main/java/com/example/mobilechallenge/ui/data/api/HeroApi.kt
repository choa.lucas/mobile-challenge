package com.example.mobilechallenge.ui.data.api

import com.example.mobilechallenge.ui.data.model.HeroResponse
import com.example.mobilechallenge.ui.data.model.HeroesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface HeroApi {

    @GET("{year}.json")
    suspend fun getHeroByYear(@Path("year") year: Int): Response<HeroesResponse>

}