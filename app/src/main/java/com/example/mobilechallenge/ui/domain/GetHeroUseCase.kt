package com.example.mobilechallenge.ui.domain

import com.example.mobilechallenge.service.Outcome
import com.example.mobilechallenge.ui.data.model.HeroResponse
import com.example.mobilechallenge.ui.data.repository.HeroRepository
import com.example.mobilechallenge.ui.domain.model.Hero
import com.example.mobilechallenge.ui.domain.model.HeroName

class GetHeroUseCase(
    private val repository: HeroRepository
) {

    suspend fun execute(year: Int): Outcome<List<Hero>> {
        return repository.getHeroByYear(year).mapResource {
            heroMapper(it.heroes)
        }
    }

    companion object {
        fun heroMapper(heroesResponse: List<HeroResponse>): List<Hero> {
            val heroes = mutableListOf<Hero>()
            heroesResponse.forEach { hero ->
                heroes.add(
                    Hero(
                        name = HeroName.fromValue(hero.name),
                        avatar = hero.avatar,
                        score = hero.score
                    )
                )
            }
            return heroes
        }
    }
}