package com.example.mobilechallenge.ui.domain.model

sealed class HeroName {
    object BATMAN : HeroName()
    object SUPERMAN : HeroName()
    object SPIDERMAN : HeroName()
    object IRONMAN : HeroName()
    object NOTFOUND : HeroName()


    companion object {
        fun fromValue(value: String) = when (value) {
            "Batman" -> BATMAN
            "Superman" -> SUPERMAN
            "Spider-Man" -> SPIDERMAN
            "Iron Man" -> IRONMAN
            else -> NOTFOUND

        }

        fun toName(hero: HeroName) = when (hero) {
            BATMAN -> "Batman"
            SUPERMAN -> "Superman"
            SPIDERMAN -> "Spider-Man"
            IRONMAN -> "Iron Man"
            else -> ""
        }
    }
}