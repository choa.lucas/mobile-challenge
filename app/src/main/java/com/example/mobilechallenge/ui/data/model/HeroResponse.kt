package com.example.mobilechallenge.ui.data.model

import com.google.gson.annotations.SerializedName

data class HeroResponse (
    @SerializedName("Name")
    val name: String,
    @SerializedName("Picture")
    val avatar: String,
    @SerializedName("Score")
    val score: Int
)