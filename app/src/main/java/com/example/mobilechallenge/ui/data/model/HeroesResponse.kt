package com.example.mobilechallenge.ui.data.model

import com.google.gson.annotations.SerializedName

data class HeroesResponse (
    @SerializedName("Heroes")
    val heroes: List<HeroResponse>
)