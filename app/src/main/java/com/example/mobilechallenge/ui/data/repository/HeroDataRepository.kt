package com.example.mobilechallenge.ui.data.repository

import com.example.mobilechallenge.service.Outcome
import com.example.mobilechallenge.service.parseResponse
import com.example.mobilechallenge.ui.data.api.HeroApi
import com.example.mobilechallenge.ui.data.model.HeroesResponse

class HeroDataRepository(
    private val service: HeroApi
) : HeroRepository {

    override suspend fun getHeroByYear(year: Int): Outcome<HeroesResponse> {
        return service.getHeroByYear(year).parseResponse()
    }
}
