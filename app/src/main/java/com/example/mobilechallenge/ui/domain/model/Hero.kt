package com.example.mobilechallenge.ui.domain.model

data class Hero(
    val name: HeroName,
    val avatar: String,
    val score: Int
)