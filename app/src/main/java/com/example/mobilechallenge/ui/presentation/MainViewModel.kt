package com.example.mobilechallenge.ui.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mobilechallenge.extensions.toLiveData
import com.example.mobilechallenge.service.Outcome
import com.example.mobilechallenge.ui.domain.GetHeroUseCase
import com.example.mobilechallenge.ui.domain.model.Hero
import kotlinx.coroutines.launch

class MainViewModel(
    private val getHeroUseCase: GetHeroUseCase
) : ViewModel() {

    private val collectedDataYear = listOf(2006, 2009, 2012, 2015, 2018)
    private var yearIndex = 0

    private var storageHero = mutableMapOf<Int, List<Hero>>()

    private var _hero = MutableLiveData<List<Hero>>()
    val hero = _hero.toLiveData()

    private var _year = MutableLiveData<String>()
    val year = _year.toLiveData()

    private var _errorCode = MutableLiveData<Int>()
    val errorCode = _errorCode.toLiveData()

    fun getNextHero(year: Int) {
        if (storageHero.containsKey(year)) {
            _hero.value = storageHero[year]
            _year.value = year.toString()
        } else {
            viewModelScope.launch {
                getHeroUseCase.execute(year).apply {
                    when (this) {
                        is Outcome.Success -> {
                            val ordered = orderByScoreAndReturnFirstThree(this.value)
                            _hero.value = ordered
                            _year.value = year.toString()
                            storageHero[year] = ordered
                        }
                        is Outcome.Error -> _errorCode.value = this.code
                    }
                }
            }
        }
    }

    fun selectNextYear(): Int {
        val nextYear = yearIndex % collectedDataYear.size
        yearIndex++

        return collectedDataYear[nextYear]
    }

    private fun orderByScoreAndReturnFirstThree(heroes: List<Hero>): List<Hero> {
        heroes.sortedBy {
            it.score
        }.also {
            return it.reversed().subList(0, 3)
        }
    }
}
