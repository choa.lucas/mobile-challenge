package com.example.mobilechallenge.ui.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilechallenge.R
import com.example.mobilechallenge.service.Service
import com.example.mobilechallenge.ui.domain.model.Hero
import com.example.mobilechallenge.ui.domain.model.HeroName
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_hero.*

class HeroesAdapter(
    private val heroes: List<Hero>
) : RecyclerView.Adapter<HeroesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_hero,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = heroes.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(heroes[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View?
            get() = itemView

        fun bindView(hero: Hero) {
//            ivAvatar.width = 120.toPx()
//            ivAvatar.height = 120.toPx()

            Picasso.get().load(configureAvatarUrl(hero.avatar)).into(ivAvatar)

            tvHeroName.text = HeroName.toName(hero.name)
            tvHeroScore.text = hero.score.toString()
        }

        private fun configureAvatarUrl(imageName: String): String {
            val image = imageName.substring(0, imageName.indexOf(EXT_JPEG))

            return "${Service.BASE_URL}$image$EXT_JPG"
        }

        companion object {
            private const val EXT_JPEG = ".jpeg"
            private const val EXT_JPG = ".jpg"
        }
    }

}