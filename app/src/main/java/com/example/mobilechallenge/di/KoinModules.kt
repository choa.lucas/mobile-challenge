package com.example.mobilechallenge.di

import com.example.mobilechallenge.service.Service
import com.example.mobilechallenge.ui.data.api.HeroApi
import com.example.mobilechallenge.ui.data.repository.HeroDataRepository
import com.example.mobilechallenge.ui.data.repository.HeroRepository
import com.example.mobilechallenge.ui.domain.GetHeroUseCase
import com.example.mobilechallenge.ui.presentation.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val serviceModules = module {
    single <HeroRepository> {
        HeroDataRepository(get())
    }

    single { Service().createService(HeroApi::class.java) }
}

val useCaseModules = module {
    single {
        GetHeroUseCase(get())
    }
}

val viewModelModules = module {
    viewModel { MainViewModel(get()) }
}