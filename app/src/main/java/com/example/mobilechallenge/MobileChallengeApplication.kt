package com.example.mobilechallenge

import android.app.Application
import com.example.mobilechallenge.di.serviceModules
import com.example.mobilechallenge.di.useCaseModules
import com.example.mobilechallenge.di.viewModelModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MobileChallengeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            //android context
            androidContext(this@MobileChallengeApplication)

            modules(listOf(serviceModules, useCaseModules, viewModelModules))
        }
    }
}