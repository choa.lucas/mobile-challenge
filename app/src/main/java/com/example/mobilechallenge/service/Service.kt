package com.example.mobilechallenge.service

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Service {

    private val builder : Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(
            OkHttpClient.Builder().addInterceptor(logInterceptor()).build()
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun <S> createService(serviceApiClass: Class<S>) : S{
        return builder.create(serviceApiClass)
    }
    private fun logInterceptor() =  HttpLoggingInterceptor {
        val mLogging = HttpLoggingInterceptor()
        mLogging.level = HttpLoggingInterceptor.Level.BODY
    }

    companion object {
        const val BASE_URL = "https://bitbucket.org/dttden/mobile-coding-challenge/raw/2ee8bd47703c62c5d217d9fb9e0306922a34e581/"
    }
}