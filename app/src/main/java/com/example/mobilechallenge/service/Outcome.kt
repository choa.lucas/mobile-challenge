package com.example.mobilechallenge.service

import java.net.HttpURLConnection

sealed class Outcome<out T >{
    data class Success < out T >(val value : T) : Outcome<T>()
    data class Error(val code : Int) : Outcome<Nothing>()

    inline fun <Y> mapResource(crossinline transform: (T) -> Y): Outcome<Y> = try {
        when (this) {
            is Success<T> -> Success(
                transform(value)
            )
            is Error -> Error(code)

        }
    } catch (e: Throwable) {
        Error(HttpURLConnection.HTTP_INTERNAL_ERROR)
    }
}