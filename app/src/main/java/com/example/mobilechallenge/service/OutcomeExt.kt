package com.example.mobilechallenge.service

import retrofit2.Response

fun <R : Any> Response<R>.parseResponse(): Outcome<R> {
    if (isSuccessful) {
        val body = body()

        if (body != null)
            return Outcome.Success(body)
    }

    return Outcome.Error(this.code())
}